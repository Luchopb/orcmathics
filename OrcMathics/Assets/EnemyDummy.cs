﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDummy : Enemy
{

    public override void Spawn()
    {
        float distanceY = Vector2.Distance(_myRow.EndPosition.position, _myRow.SpawnPosition.position);
        var _endPositionY = transform.position.y - Mathf.Abs(distanceY / 3);
        transform.position = new Vector3(transform.position.x, _endPositionY, transform.position.z);
    }
    
    void Update()
    {
        
    }

    public override void KillEnemy(bool giveScore = false)
    {
        PlaySound(_deathClip);
        _anim.SetTrigger("Death");
    }

    public override void Die()
    {
        transform.localPosition = Vector3.zero;
        _myRow.ReleaseRow();
        _myRow = null;
        _aimedAt = false;
        gameObject.SetActive(false);
    }
}
