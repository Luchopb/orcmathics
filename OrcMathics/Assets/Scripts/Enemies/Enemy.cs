﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected float _speed;
    [SerializeField] private int _damage = 1;
    [SerializeField] protected float _arrivalDistance = 0.25f;
    [SerializeField] protected int _rowsUsed;
    [SerializeField] private int _baseScore;
    [SerializeField] private EnemyClass _class;
    [Header("Audio Settings")]
    [SerializeField] protected AudioClip _spawnClip;
    [SerializeField] protected AudioClip _walkClip;
    [SerializeField] protected AudioClip _deathClip;
    [SerializeField] protected AudioClip _castClip;

    protected RowController _myRow;
    protected bool _inUse;
    protected bool _aimedAt;
    protected bool _canMove = true;
    protected bool _dead;
    protected int _levelIndex;
    protected float _actualDistance;
    protected Animator _anim;
    protected AudioSource _myAudioSource;


    public int BaseScore { get { return _baseScore; } }
    public int LevelIndex { get { return _levelIndex; } }
    public bool InUse { get { return _inUse; } set { _inUse = value; } }
    public bool AimedAt { get { return _aimedAt; } set { _aimedAt = value; } }
    public bool Dead { get { return _dead; } }
    public EnemyClass Class { get { return _class; } }
    public float ArrivalDistance { get { return _arrivalDistance; } }

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _myAudioSource = GetComponent<AudioSource>();
    }

    protected virtual void Start()
    {
        GameManager.Instance.PauseEvent.AddListener(Pause);
        GameManager.Instance.ResumeEvent.AddListener(Resume);
    }

    private void Update()
    {
        if (!_inUse || !_canMove) return;

        _actualDistance = Vector2.Distance(transform.position, _myRow.EndPosition.transform.position);

        if (_actualDistance > _arrivalDistance)
            transform.position += (_myRow.EndPosition.transform.position - transform.position).normalized * _speed * Time.deltaTime;
        else
            Attack();
    }

    public virtual void SetEnemy(RowController row, int index)
    {
        _inUse = true;
        _myRow = row;
        _myRow.OccupyRow(this);
        _levelIndex = index;
    }

    public virtual void Cast() { }

    public virtual void StartMoving()
    {
        _canMove = true;
        PlaySound(_walkClip, true);
    }

    public virtual bool CanSpawn()
    {
        return RowManager.Instance.GetAvailableRowsAmount(LevelManager.Instance.GetActualLevelRange()) >= _rowsUsed;
    }

    public virtual void Spawn()
    {
        _dead = false;
        //_canMove = true;
        PlaySound(_spawnClip);
        Invoke(nameof(StartMoving), _spawnClip != null ? _spawnClip.length : 0);
    }

    protected IEnumerator PlaySoundAfterDelay(float delay, AudioClip clip, bool loop)
    {
        yield return new WaitForSeconds(delay);
        PlaySound(clip, loop);
    }

    public virtual void Attack()
    {
        GameManager.Instance.DamagePlayer(_damage);
        KillEnemy(false);
    }

    public virtual void KillEnemy(bool giveScore = false)
    {
        _dead = true;
        _canMove = false;
        if (giveScore)
        {
            PlaySound(_deathClip);
            _anim.SetTrigger("Death");
            LevelManager.Instance.EnemyKilled();
            GameManager.Instance.AddScore(CalculateScore());
        }
        else
        {
            Die();
            LevelManager.Instance.ReturnEnemyToQueue(_levelIndex);
        }
    }

    public virtual void Die()
    {
        PauseSound();
        _myRow.ReleaseRow();
        _aimedAt = false;
        UnitPoolManager.Instance.ReturnEnemy(this);
        _canMove = true;
    }

    public int CalculateScore()
    {
        return _baseScore * Mathf.RoundToInt(Vector2.Distance(transform.position, _myRow.EndPosition.transform.position));
    }

    public void PlaySound(AudioClip clip, bool loop = false)
    {
        _myAudioSource.Pause();
        _myAudioSource.clip = clip;
        _myAudioSource.loop = loop;
        _myAudioSource.Play();
    }

    public void PauseSound()
    {
        _myAudioSource.Pause();
    }

    public void Pause()
    {
        PauseSound();
        _canMove = false;
    }

    public void Resume()
    {
        if (!isActiveAndEnabled) return;
        _myAudioSource.Play();
        _canMove = true;
    }
}

[System.Serializable]
public class EnemyData
{
    [HideInInspector] public bool Used;
    public EnemyClass Enemy;
}

public enum EnemyClass { Warrior, Goblin, Rogue, Shaman, Monk, Rider, Warchief, Guard, Boss, Dummy, Error }
