﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyBoss : Enemy
{
    [SerializeField] private int _maxLifes = 5;

    private bool _originalPlayerSign;

    public override void Spawn()
    {
        base.Spawn();
        _maxLifes = 5;
        _originalPlayerSign = PlayerController.Instance.PositiveSign;
    }

    public override void KillEnemy(bool giveScore = false)
    {
        if (giveScore)
            _maxLifes--;
        else
        {
            PlayerController.Instance.PositiveSign = !_originalPlayerSign;
            PlayerController.Instance.ChangeSign();
            _maxLifes = 5;
            base.KillEnemy(giveScore);
            return;
        }

        if (_maxLifes > 0)
        {
            PlaySound(_castClip);
            _anim.SetTrigger("Hit");
            Invoke(nameof(ChangePlayerSign), 3f);
            _canMove = false;
            _myRow.ReleaseRow();
            var newRow = RowManager.Instance.GetAvailableRow(LevelManager.Instance.GetActualLevelRange());

            if (newRow != null)
                _myRow = newRow;

            _myRow.OccupyRow(this);
        }
        else
        {
            _maxLifes = 5;
            base.KillEnemy(giveScore);
        }
    }

    public void ChangePlayerSign()
    {
        PlayerController.Instance.ChangeSign();
    }

    public void Reposition()
    {
        transform.DOMove(new Vector3(_myRow.SpawnPosition.position.x, _myRow.SpawnPosition.position.y, transform.position.z), 2f).OnComplete(Land);
    }

    public void Land()
    {
        _anim.SetTrigger("Spawn");
        PlaySound(_spawnClip);
        StartCoroutine(PlaySoundAfterDelay(_spawnClip.length, _walkClip, true));
        _canMove = true;
        _aimedAt = false;
    }
}
