﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFakeMonk : Enemy
{
    public override void SetEnemy(RowController row, int index)
    {
        _canMove = true;
        gameObject.SetActive(true);
        _anim.SetTrigger("Spawn");

        base.SetEnemy(row, index);

        _aimedAt = false;
        transform.position = new Vector3(_myRow.transform.position.x, transform.position.y, transform.position.z);
    }

    private void Update()
    {

    }

    public override void Attack()
    {
    }

    public override void KillEnemy(bool giveScore = false)
    {
        _canMove = false;
        _anim.SetTrigger("Death");
    }

    public override void Die()
    {
        transform.localPosition = Vector3.zero;
        _myRow.ReleaseRow();
        _myRow = null;
        _aimedAt = false;
        gameObject.SetActive(false);
    }
}
