﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class EnemyGoblin : Enemy
{
    [SerializeField] private AudioClip _barrelRollClip;
    [SerializeField] private AudioClip _barrelBlockClip;
    [SerializeField] private Animator _barrelBlockAnim;

    private float _endPositionY;
    private bool _canBeDamaged;

    public override void Spawn()
    {
        PlaySound(_barrelRollClip, true);
        _aimedAt = false;
        _canMove = false;
        _anim.SetBool("Barreling", true);
        _anim.SetTrigger("Barrel");

        float distanceY = Vector2.Distance(_myRow.EndPosition.position, _myRow.SpawnPosition.position);
        _endPositionY = transform.position.y - Mathf.Abs(distanceY / 3);
        transform.DOMoveY(_endPositionY, 2.5f).OnComplete(BreakBarrel);
    }

    public void BreakBarrel()
    {
        PlaySound(_spawnClip);
        _canMove = true;
        _anim.SetBool("Barreling", false);
    }

    public override void StartMoving()
    {
        _canBeDamaged = true;
    }

    public override void KillEnemy(bool giveScore = false)
    {
        if (!_canBeDamaged)
        {
            _barrelBlockAnim.SetTrigger("Hit");
            AudioManager.Instance.PlaySound(_barrelBlockClip);
            _aimedAt = false;
            return;
        }
        _canBeDamaged = false;
        base.KillEnemy(giveScore);
    }
}
