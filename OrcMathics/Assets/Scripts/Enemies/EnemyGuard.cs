﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGuard : Enemy
{
    private EnemyGuard _myBrother;

    public override void SetEnemy(RowController row, int index)
    {
        gameObject.SetActive(true);
        transform.SetParent(transform.parent.parent);
        base.Spawn();

        base.SetEnemy(row, index);

        _aimedAt = false;
        transform.localPosition = new Vector3(_myRow.transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
    }

    public void SetBrother(EnemyGuard guard)
    {
        _myBrother = guard;
    }

    public void StartWalkAlone()
    {
        _anim.SetBool("Alone", true);
    }

    public override void KillEnemy(bool giveScore = false)
    {
        _dead = true;
        PlaySound(_deathClip);

        if (giveScore)
        {
            _myBrother.StartWalkAlone();
            _anim.SetTrigger("Death");
        }
        else
            Die();
    }

    public override void Die()
    {
        _myRow.ReleaseRow();
        _myRow = null;
        _aimedAt = false;
        gameObject.SetActive(false);
    }
}
