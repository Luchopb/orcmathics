﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMonk : Enemy
{
    [SerializeField] private float _timeToMultiply = 3f;
    [SerializeField] private EnemyFakeMonk[] _fakeClones;

    private List<RowController> _rowsToUse;
    private float _initialSpeed;

    public override void Spawn()
    {
        base.Spawn();
        _initialSpeed = _speed;
        _speed = _initialSpeed * 2;
        for (int i = 0; i < _fakeClones.Length; i++)
        {
            _fakeClones[i].AimedAt = true;
            _fakeClones[i].gameObject.SetActive(false);
        }

        _rowsToUse = RowManager.Instance.GetAvailableRows(LevelManager.Instance.GetActualLevelRange(), _rowsUsed - 1);
        //Ocupo las rows para que no me las saquen,
        //Y las inmunizo temporalmente asi no me pegan desde ellas
        for (int i = 0; i < _rowsToUse.Count; i++)
            _rowsToUse[i].OccupyRow(_fakeClones[i]);

        if(_rowsToUse != null)
            Invoke(nameof(Multiply), _timeToMultiply);
        else
            Invoke(nameof(Spawn), 1f);
    }

    public override void Cast()
    {
        base.Cast();

        _rowsToUse.Add(_myRow);

        var newRow = _rowsToUse[Random.Range(0, _rowsToUse.Count)];
        _myRow.ReleaseRow();
        _myRow = newRow;
        _myRow.OccupyRow(this);
        _aimedAt = false;
        transform.localPosition = new Vector3(_myRow.transform.localPosition.x, transform.localPosition.y, transform.position.z);
        //_anim.SetTrigger("Spawn");

        _rowsToUse.Remove(newRow);

        for (int i = 0; i < _fakeClones.Length; i++)
        {
            _fakeClones[i].AimedAt = false;
            _fakeClones[i].SetEnemy(_rowsToUse[i], _levelIndex);
        }
    }

    //Activa sus clones
    public void Multiply()
    {
        if (_aimedAt) return;
        _speed = _initialSpeed;
        _aimedAt = true;
        _anim.SetTrigger("Cast");
        PlaySound(_castClip);
        StartCoroutine(PlaySoundAfterDelay(_castClip.length, _walkClip, true));
    }

    public override void KillEnemy(bool giveScore = false)
    {
        CancelInvoke(nameof(Multiply));

        _speed = _initialSpeed;
        for (int i = 0; i < _rowsToUse.Count; i++)
            _rowsToUse[i].ReleaseRow();

        for (int i = 0; i < _fakeClones.Length; i++)
        {
            if(_fakeClones[i].isActiveAndEnabled)
                _fakeClones[i].KillEnemy();
        }

        base.KillEnemy(giveScore);
    }
}
