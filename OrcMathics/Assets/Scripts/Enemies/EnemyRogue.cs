﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRogue : Enemy
{
    [SerializeField] private float _vanishCooldown;

    public override void Spawn()
    {
        base.Spawn();
        InvokeRepeating(nameof(Vanish), _vanishCooldown, _vanishCooldown);
    }

    //Agarra una row libre y se tepea a ella
    //Mantiene su posicion en Y, solo se enlista a esa row y toma su posicion en X
    public void Vanish()
    {
        if (_aimedAt || _actualDistance < 3f) return;

        var newRow = RowManager.Instance.GetAvailableRow(LevelManager.Instance.GetActualLevelRange());

        if (newRow == null) return;

        _canMove = false;
        _myRow.ReleaseRow();
        _myRow = newRow;
        _myRow.OccupyRow(this);
        _anim.SetTrigger("Cast");
        PlaySound(_castClip);
        StartCoroutine(PlaySoundAfterDelay(_castClip.length, _walkClip, true));
    }

    public override void Cast()
    {
        _myRow.OccupyRow(this);
        _aimedAt = false;

        transform.localPosition = new Vector3(_myRow.transform.localPosition.x, transform.localPosition.y, transform.position.z);
        base.Cast();
    }

    public override void StartMoving()
    {
        base.StartMoving();
        _canMove = true;
    }

    public override void KillEnemy(bool giveScore = false)
    {
        CancelInvoke(nameof(Vanish));
        base.KillEnemy(giveScore);
    }
}
