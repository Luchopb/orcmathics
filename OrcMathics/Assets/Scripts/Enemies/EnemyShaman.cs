﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShaman : Enemy
{
    [SerializeField] private float _timeToProtectRows = 6f;
    [SerializeField] private int _shieldDistance = 2;
    private int _startIndex, _endIndex;

    public override void Spawn()
    {
        base.Spawn();
        Invoke(nameof(MakeRowsImmune), _timeToProtectRows);
    }

    public override void Cast()
    {
        if (!isActiveAndEnabled || !_inUse || _dead) return;

        base.Cast();

        //-------------------------------------- ORIGINAL

        //for (int i = 0; i < RowManager.Instance.AllRows.Count; i++)
        //{
        //    if (RowManager.Instance.AllRows[i] != _myRow)
        //        RowManager.Instance.AllRows[i].MakeImmune();
        //}

        //--------------------------------------

        int dist = _shieldDistance;

        _startIndex = _myRow.RowIndex + 7 - _shieldDistance;
        _endIndex = _myRow.RowIndex + 7 + _shieldDistance;

        if (_startIndex < 0)
        {
            //correccion de inicio si estoy muy al borde
            do
                _startIndex = _myRow.RowIndex + 7 - dist--;
            while (_startIndex < 0);
        }
        else if (_endIndex > 14) //ajuste de final si estoy muy al borde
        {
            do
                _endIndex = _myRow.RowIndex + 7 + dist--;
            while (_endIndex > 14);
        }

        for (int i = _startIndex; i <= _endIndex; i++)
        {
            if (RowManager.Instance.AllRows[i] != _myRow)
                RowManager.Instance.AllRows[i].MakeImmune();
        }

        //--------------------------------------

        _canMove = true;
    }

    public void MakeRowsImmune()
    {
        _canMove = false;
        _anim.SetTrigger("Cast");
        PlaySound(_castClip);
        if(isActiveAndEnabled)
            StartCoroutine(PlaySoundAfterDelay(_castClip.length, _walkClip, true));
    }

    public override void KillEnemy(bool giveScore = false)
    {
        base.KillEnemy(giveScore);
        for (int i = _startIndex; i <= _endIndex; i++)
        {
            if (RowManager.Instance.AllRows[i] != _myRow)
                RowManager.Instance.AllRows[i].TurnOffImmune();
        }
    }
}
