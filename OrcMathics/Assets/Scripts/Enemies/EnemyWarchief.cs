﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWarchief : Enemy
{
    [SerializeField] private AudioClip _blockClip;
    [SerializeField] private Animator _blockAnim;
    [SerializeField] private EnemyGuard[] _guards;

    private List<RowController> _rowsToUse;
    private Vector3[] _guardsLocalPositions;

    //Overrideo el del padre, y retorno el doble si no puede ser dañado para que choque con el tronco
    public new float ArrivalDistance { get { return CanBeDamaged() ? _arrivalDistance : _arrivalDistance * 2; } }

    public override bool CanSpawn()
    {
        _rowsToUse = RowManager.Instance.GetSequentialRows(LevelManager.Instance.GetActualLevelRange(), _rowsUsed);
        if (_rowsToUse != null)
        { 
            for (int i = 0; i < _rowsToUse.Count; i++)
            {
                _rowsToUse[i].OccupyRow(this);
            }
        }
        return _rowsToUse != null;
    }

    public override void Spawn()
    {
        base.Spawn();
        _guardsLocalPositions = new Vector3[_guards.Length];
        _myRow.ReleaseRow();
        _myRow = _rowsToUse[1];
        _myRow.OccupyRow(this);
        transform.localPosition = new Vector3(_myRow.transform.localPosition.x, transform.localPosition.y, transform.position.z);

        _rowsToUse.Remove(_myRow);

        for (int i = 0; i < _guards.Length; i++)
        {
            _guardsLocalPositions[i] = _guards[i].transform.localPosition;
            _guards[i].SetEnemy(_rowsToUse[i], _levelIndex);
        }

        //Seteo los hermanos de los guardias
        _guards[1].SetBrother(_guards[0]);
        _guards[0].SetBrother(_guards[1]);
    }

    public bool CanBeDamaged()
    {
        for (int i = 0; i < _guards.Length; i++)
            if (!_guards[i].Dead)
                return false;

        return true;
    }

    public override void KillEnemy(bool giveScore = false)
    {
        _aimedAt = false;
        if (giveScore)
            if (!CanBeDamaged())
            {
                AudioManager.Instance.PlaySound(_blockClip);
                _blockAnim.SetTrigger("Hit");
                return;
            }

        for (int i = 0; i < _guards.Length; i++)
        {
            _guards[i].transform.SetParent(transform);
            _guards[i].transform.localPosition = _guardsLocalPositions[i];

            if (_guards[i].isActiveAndEnabled)
                _guards[i].Die();
        }

        base.KillEnemy(giveScore);
    }
}
