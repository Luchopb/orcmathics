﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class InitialLevelTutorial : LevelTutorial
{
    [Header("Initial Tutorial Settings")]
    [SerializeField] private Text _kingBubbleText;
    [SerializeField] private Image _kingBubbleImage;
    [SerializeField] private Text _soldierBubbleText;
    [SerializeField] private Image _soldierBubbleImage;
    [Header("Arrows")]
    [SerializeField] private Image _rouletteArrowImage;
    [SerializeField] private Image _goArrowImage;
    [Header("Player Canvas")]
    [SerializeField] private Button _goButton;
    [SerializeField] private ScrollRect _numberRoulette;
    //[SerializeField]

    private int _dummyCounter;
    private bool _aimedAtRow;
    private IEnumerator _fadeOutTextCoroutine;

    protected override void Start()
    {
        base.Start();
        PlayerController.Instance.ChoooseRowEvent.AddListener(CheckAimedRow);
        PlayerController.Instance.EnemyKilledEvent.AddListener(DummyKilled);
    }
    
    public override void Hide()
    {
    }
    
    public void StartTutorialLevel()
    {
        PlayerController.Instance.Resume();
        gameObject.SetActive(true);
        _kingTransform.DOLocalMoveX(_finalKingPositionX, 1f).SetEase(Ease.OutQuart);
        _goButton.interactable = false;
        _backgroundButton.interactable = false;
        _backgroundImage.raycastTarget = false;
        FadeInObject(_kingBubbleText, _kingBubbleImage);
        UpdateText(_kingBubbleText, "tut_king_1", "speech_tut_king_1");
        FadeInObject(null, _rouletteArrowImage);
    }

    public void CheckAimedRow(RowController aimedRow)
    {
        if (aimedRow != null)
        {
            if (_aimedAtRow) return;

            if (DOTween.IsTweening(_kingBubbleText))
                DOTween.Kill(_kingBubbleText);

            FadeInObject(_kingBubbleText, _kingBubbleImage);
            _goButton.interactable = true;
            _numberRoulette.enabled = false;
            _aimedAtRow = true;
            UpdateText(_kingBubbleText, "tut_king_2", "speech_tut_king_2");
            FadeOutObject(null, _rouletteArrowImage);
            FadeInObject(null, _goArrowImage);
        }
        else if(_aimedAtRow)
        {
            if (DOTween.IsTweening(_kingBubbleText))
                DOTween.Kill(_kingBubbleText);

            FadeInObject(_kingBubbleText, _kingBubbleImage);
            _goButton.interactable = false;
            _numberRoulette.enabled = true;
            _aimedAtRow = false;
            UpdateText(_kingBubbleText, "tut_king_1", "speech_tut_king_1");
            FadeOutObject(null, _goArrowImage);
            FadeInObject(null, _rouletteArrowImage);
        }
    }

    public void DummyKilled()
    {
        PlayerController.Instance.ChoooseRowEvent.RemoveAllListeners();
        _dummyCounter++;

        if (_dummyCounter == 1)
        {
            if (DOTween.IsTweening(_kingBubbleText))
                DOTween.Kill(_kingBubbleText);

            FadeInObject(_kingBubbleText, _kingBubbleImage);
            _numberRoulette.enabled = true;
            FadeOutObject(null, _goArrowImage);
            FadeOutObject(null, _rouletteArrowImage);
            UpdateText(_kingBubbleText, "tut_king_3", "speech_tut_king_3");

        }
        else if (_dummyCounter == 3)
        {
            PlayerController.Instance.EnemyKilledEvent.RemoveListener(DummyKilled);
            Invoke(nameof(StartSoldierDialogue), 0.5f);
        }
    }

    public void StartSoldierDialogue()
    {
        FadeInObject(_soldierBubbleText, _soldierBubbleImage);
        UpdateText(_soldierBubbleText, "tut_soldier", "speech_tut_soldier");
        Invoke(nameof(StartKingResponse), 3f);
    }

    public void StartKingResponse()
    {
        FadeOutObject(_soldierBubbleText, _soldierBubbleImage);
        FadeInObject(_kingBubbleText, _kingBubbleImage);
        UpdateText(_kingBubbleText, "tut_king_4", "speech_tut_king_4");
        Invoke(nameof(StartGame), 5f);
    }

    public void StartGame()
    {
        _kingTransform.DOLocalMoveX(_initialKingPositionX, 1f).SetEase(Ease.OutQuart).OnComplete(LevelManager.Instance.StartLevel);
        FadeOutObject(_kingBubbleText, _kingBubbleImage);
    }

    public void FadeInObject(Text textToFade = null, Image imageToFade = null)
    {
        if (imageToFade != null)
            imageToFade.DOFade(1, 0.5f);

        if (textToFade!=null)
            textToFade.DOFade(1, 0.5f);
    }

    public void FadeOutObject(Text textToFade = null, Image imageToFade = null)
    {
        if (imageToFade != null)
            imageToFade.DOFade(0, 0.5f);

        if (textToFade != null)
            textToFade.DOFade(0, 0.5f);
    }

    public void UpdateText(Text textToUpdate, string textKey, string speechKey = "")
    {
        Sequence textSeq = DOTween.Sequence();
        textSeq.Append(textToUpdate.DOFade(0, 0.25f).OnComplete(() => textToUpdate.text = LoLManager.Instance.LanguageTexts[textKey]));
        textSeq.Append(textToUpdate.DOFade(1, 0.5f));

        if (!string.IsNullOrEmpty(speechKey))
            LoLManager.Instance.SpeechToText(speechKey);
    }

    IEnumerator FadeOutAfterDelay(float delay, Text textToFade = null, Image imageToFade = null)
    {
        yield return new WaitForSeconds(delay);
        _goArrowImage.DOFade(0, 0.5f);
        _rouletteArrowImage.DOFade(0, 0.5f);
        FadeOutObject(textToFade, imageToFade);
    }
}
