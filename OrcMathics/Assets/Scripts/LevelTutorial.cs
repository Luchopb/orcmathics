﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LevelTutorial : MonoBehaviour
{
    [Header("Black Screen Settings")]
    [SerializeField] protected Transform _kingTransform;
    [SerializeField] protected Transform _paperTransform;
    [SerializeField] protected string _speechKey;
    [SerializeField] protected Text _mainText;
    [SerializeField] protected string _mainTextKey;
    [SerializeField] protected Text _bodyText;
    [SerializeField] protected string _bodyTextKey;
    [SerializeField] protected float _finalKingPositionX;
    [SerializeField] protected float _finalPaperPositionY;

    protected Image _backgroundImage;
    protected Button _backgroundButton;
    protected bool _shown;
    protected float _initialKingPositionX;
    protected float _initialPaperPositionY;

    public bool Shown { get { return _shown; } }

    protected void Awake()
    {
        _backgroundImage = GetComponent<Image>();
        _backgroundButton = GetComponent<Button>();

        _backgroundButton.onClick.AddListener(Hide);

        gameObject.SetActive(false);
    }

    protected virtual void Start()
    {
        _initialKingPositionX = _kingTransform.localPosition.x;
        _initialPaperPositionY = _paperTransform.localPosition.y;

        _mainText.text = LoLManager.Instance.LanguageTexts[_mainTextKey];
        _bodyText.text = LoLManager.Instance.LanguageTexts[_bodyTextKey];
    }

    private void Update()
    {
        if (!isActiveAndEnabled || _shown) return;

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            Hide();
    }

    public void Show()
    {
        gameObject.SetActive(true);
        GameManager.Instance.PauseEvent.Invoke();
        Sequence showSequence = DOTween.Sequence().OnComplete(() => LoLManager.Instance.SpeechToText(_speechKey));
        showSequence.Append(_backgroundImage.DOFade(0.8f, 1f));
        _kingTransform.DOLocalMoveX(_finalKingPositionX, 1f).SetEase(Ease.OutQuart);
        _paperTransform.DOLocalMoveY(_finalPaperPositionY, 1f).SetEase(Ease.OutQuart);
        showSequence.Append(_mainText.DOFade(1f, 0.5f));
        showSequence.Append(_bodyText.DOFade(1f, 0.5f));
    }

    public virtual void Hide()
    {
        LoLManager.Instance.SpeechToText("stop_speech");
        Sequence hideSequence = DOTween.Sequence().OnComplete(ResumeGame);
        hideSequence.Append(_mainText.DOFade(0, 1f));
        _bodyText.DOFade(0f, 1f);
        _paperTransform.DOLocalMoveY(_initialPaperPositionY, 1f).SetEase(Ease.OutQuart);
        _kingTransform.DOLocalMoveX(_initialKingPositionX, 1f).SetEase(Ease.OutQuart);
        _backgroundImage.DOFade(0, 1f);
    }

    public void ResumeGame()
    {
        _shown = true;
        LevelManager.Instance.StartLevel();
        //GameManager.Instance.ResumeEvent.Invoke();
        gameObject.SetActive(false);
    }
}
