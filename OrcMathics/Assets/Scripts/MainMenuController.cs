﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenuController : MonoBehaviour
{
    [Header("Audio Settings")]
    [SerializeField] private AudioClip _playGameClip;
    [SerializeField] private AudioClip _bossFallClip;
    [SerializeField] private AudioClip _buttonAppearClip;
    [SerializeField] private AudioClip _mainThemeClip;
    [Header("Image Settings")]
    [SerializeField] private Image _titleImage;
    [SerializeField] private Image _bossImage;
    [SerializeField] private float _bossEndPosX;
    [SerializeField] private float _bossEndPosY;
    [SerializeField] private float _timeTofall;
    [SerializeField] private float _yHoverOffSet;
    [SerializeField] private float _timeToLoopGlow;
    [SerializeField] private Image _glowImage;
    [SerializeField] private Button _playbutton;

    private bool _canStart;

    private void Start()
    {
        StartGlowLoop();
        _playbutton.onClick.AddListener(StartGame);
        AudioManager.Instance.PlaySound(_mainThemeClip, 0, AudioChannels.BackgroundMusic);
        
        _bossImage.transform.DOLocalMoveX(_bossEndPosX, _timeTofall).SetEase(Ease.OutQuad).OnComplete(StartHoverLoop).SetDelay(0.2f);
        _bossImage.transform.DOScale(new Vector3(1, 1, 1), _timeTofall);
        Invoke(nameof(Roar), _timeTofall / 3.5f);
    }

    private void Update()
    {
        if (!_canStart) return;

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            StartGame();
    }

    public void Roar()
    {
        AudioManager.Instance.PlaySound(_bossFallClip, 0.2f);
    }

    public void StartHoverLoop()
    {
        _bossImage.transform.DOLocalMoveY(_bossEndPosY + _yHoverOffSet, 2f).SetEase(Ease.InOutQuad).SetLoops(int.MaxValue, LoopType.Yoyo);

        Sequence uiSequence = DOTween.Sequence().OnComplete(() => _canStart = true);
        uiSequence.Append(_titleImage.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnStart(() => AudioManager.Instance.PlaySound(_buttonAppearClip, 1)));
        uiSequence.Append(_playbutton.transform.DOScale(1, 0.5f).SetEase(Ease.OutBack).OnStart(() => AudioManager.Instance.PlaySound(_buttonAppearClip, 1)));
    }

    public void StartGlowLoop()
    {
        Sequence glowSeq = DOTween.Sequence().SetLoops(int.MaxValue, LoopType.Restart);
        glowSeq.Append(_glowImage.DOFade(0.5f, _timeToLoopGlow).SetEase(Ease.InOutQuad));
        glowSeq.Append(_glowImage.DOFade(1f, _timeToLoopGlow).SetEase(Ease.InOutQuad));

        Sequence scaleSeq = DOTween.Sequence().SetLoops(int.MaxValue, LoopType.Restart);
        scaleSeq.Append(_glowImage.transform.DOScale(0.75f, _timeToLoopGlow).SetEase(Ease.InOutQuad));
        scaleSeq.Append(_glowImage.transform.DOScale(1f, _timeToLoopGlow).SetEase(Ease.InOutQuad));

        _glowImage.transform.DORotate(new Vector3(0f, 0f, 180f), _timeToLoopGlow * 4f).SetEase(Ease.Linear).SetLoops(int.MaxValue, LoopType.Incremental);
    }

    public void StartGame()
    {
        AudioManager.Instance.PlaySound(_playGameClip);
        SceneLoader.Instance.GoToGame();
    }
}
