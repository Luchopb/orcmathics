﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }

    [SerializeField] private AudioClip _mainTheme;
    [SerializeField] [Range(0,1)]
    private float _defaultVolume = 0.8f;

    private List<AudioSource> _audioSources;

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;
        DontDestroyOnLoad(gameObject);

        var initialAudios = GetComponents<AudioSource>();

        _audioSources = new List<AudioSource>(initialAudios);

        _audioSources[0].loop = true;
        for (int i = 1; i < _audioSources.Count; i++)
        {
            InitialSetUp(_audioSources[i]);
        }

        PlaySound(_mainTheme, _defaultVolume, AudioChannels.BackgroundMusic);
    }

    public void InitialSetUp(AudioSource audio)
    {
        audio.loop = false;
        audio.volume = _defaultVolume;
    }

    public void PlaySound(AudioClip clip, float clipVolume = 0, AudioChannels channel = AudioChannels.SoundEffect, bool generateNewSource = true)
    {
        if (channel == AudioChannels.BackgroundMusic)
        {
            _audioSources[0].Pause();
            _audioSources[0].clip = clip;
            _audioSources[0].Play();
        }
        else
        {
            //Recorro los audios hasta encontrar uno disponible
            for (int i = 1; i < _audioSources.Count; i++)
            {
                if(!_audioSources[i].isPlaying)
                {
                    _audioSources[i].clip = clip;
                    if (clipVolume != 0)
                        _audioSources[i].volume = clipVolume;
                    else
                        _audioSources[i].volume = _defaultVolume;
                    _audioSources[i].Play();
                    return;
                }
            }
            if(!generateNewSource)
                return;

            //Si no hay audio disponible, genero uno
            var auxSource = gameObject.AddComponent<AudioSource>();
            _audioSources.Add(auxSource);
            InitialSetUp(auxSource);
            auxSource.clip = clip;
            if (clipVolume != 0)
                auxSource.volume = clipVolume;
            else
                auxSource.volume = _defaultVolume;
            auxSource.Play();
        }
    }

    public void PlayLoopedSound(AudioClip clip, float timeToLoop, AudioChannels channel = AudioChannels.SoundEffect, bool generateNewSource = true, float clipVolume = 0)
    {
        //Recorro los audios hasta encontrar uno disponible
        for (int i = 1; i < _audioSources.Count; i++)
        {
            if (!_audioSources[i].isPlaying)
            {
                _audioSources[i].clip = clip;
                _audioSources[i].loop = true;
                if (clipVolume != 0)
                    _audioSources[i].volume = clipVolume;
                _audioSources[i].Play();
                
                StartCoroutine(SetLoopOff(_audioSources[i], timeToLoop));
                return;
            }
        }
        if (!generateNewSource)
            return;

        //Si no hay audio disponible, genero uno
        var auxSource = gameObject.AddComponent<AudioSource>();
        _audioSources.Add(auxSource);
        InitialSetUp(auxSource);
        auxSource.clip = clip;
        auxSource.loop = true;
        if (clipVolume != 0)
            auxSource.volume = clipVolume;
        auxSource.Play();

        StartCoroutine(SetLoopOff(auxSource, timeToLoop));
    }

    IEnumerator SetLoopOff(AudioSource source, float delay)
    {
        yield return new WaitForSeconds(delay);
        source.loop = false;
    }
}


//A futuro
public class AudioData
{
    public string Name;
    public AudioClip Clip;
    public bool Loop;
    [Range(0, 1)]
    public float Volume;
}

public enum AudioChannels { BackgroundMusic = 0, SoundEffect = 1}
