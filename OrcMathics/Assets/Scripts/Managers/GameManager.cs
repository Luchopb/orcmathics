﻿using DG.Tweening;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [Header("Audio Settings")]
    [SerializeField] private AudioClip _backgroundMusic;
    [SerializeField] private AudioClip _lifeDownClip;
    [SerializeField] private AudioClip _winClip;
    [SerializeField] private AudioClip _loseClip;
    [SerializeField] private AudioClip _pauseClip;
    [Header("Player Settings")]
    [SerializeField] private int _playerMaxLives;
    [SerializeField] private Image[] _playerLivesImages;
    [SerializeField] private Text _playerScoreText;
    [Header("End Screen Settings")]
    [SerializeField] private Image _endScreen;
    [SerializeField] private Image _winImage;
    [SerializeField] private Image _kingImage;
    [SerializeField] private Text _winTitle;
    [SerializeField] private Text _endWinScore;
    [SerializeField] private Image _loseImage;
    [SerializeField] private Text _loseTitle;
    [SerializeField] private Text _endLoseScore;
    [SerializeField] private Button _winButton;
    [SerializeField] private Button _loseButton;
    [SerializeField] private float _timeToFadeScreen;
    [Header("UI Settings")]
    [SerializeField] private GameObject _rowContainer;
    [SerializeField] private GameObject _tutorialScreen;
    [Header("Canvas")]
    [SerializeField] private GameObject _focusBg;
    [SerializeField] private Text _focusText;
    [SerializeField] private Canvas _mainCanvas;
    [SerializeField] private float _timeToShake;
    [SerializeField] private float _shakingForce;

    private int _actualPlayerLives;
    private int _actualMaxScore;
    private int _lastLevelScore;
    private int _actualScore;
    private UnityEvent _pauseEvent;
    private UnityEvent _resumeEvent;

    private bool _gameEnded;
    private bool _winner;
    public UnityEvent PauseEvent { get { return _pauseEvent; } }
    public UnityEvent ResumeEvent { get { return _resumeEvent; } }

    Sequence _endScreenSequence;

    public int ActualMaxScore
    {
        get
        {
            _lastLevelScore = _actualMaxScore;
            return _actualMaxScore;
        }
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        _focusBg.SetActive(!hasFocus);
    }

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;

        _pauseEvent = new UnityEvent();
        _resumeEvent = new UnityEvent();

        _endScreen.gameObject.SetActive(false);
        _actualPlayerLives = _playerMaxLives;
        _actualScore = _actualMaxScore = LoLManager.Instance._data.score;
        _playerScoreText.text = _actualScore.ToString("00000");
    }

    private void Start()
    {
        //Time.timeScale = 2;
        //PlayerPrefs.SetInt("Tutorial", 0);
        _tutorialScreen.gameObject.SetActive(false);
        AudioManager.Instance.PlaySound(_backgroundMusic, 0, AudioChannels.BackgroundMusic);

        _loseButton.onClick.AddListener(ResetLevel);

        if (PlayerPrefs.GetInt("Tutorial", 0) == 1)
            LevelManager.Instance.StartLevel();
        else
            _tutorialScreen.gameObject.SetActive(true);

        //_playerScoreTitle.text = LoLManager.Instance.LanguageTexts["score"];
        //_progressBarTitle.text = LoLManager.Instance.LanguageTexts["smashed"];
        _playerScoreText.text = LoLManager.Instance.LanguageTexts["score"] + ": " + _actualScore.ToString("00000");
        _endWinScore.text = LoLManager.Instance.LanguageTexts["score"] + ": ";
        _endLoseScore.text = LoLManager.Instance.LanguageTexts["score"] + ": ";
        _focusText.text = LoLManager.Instance.LanguageTexts["resume"];
        _winTitle.text = LoLManager.Instance.LanguageTexts["win"];
        _loseTitle.text = LoLManager.Instance.LanguageTexts["lose"];
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.W))
            EndGame(true);*/

        if (!_gameEnded) return;

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            if (_winner)
                _winButton.onClick.Invoke();
            else
                _loseButton.onClick.Invoke();
        }
    }

    public void DamagePlayer(int damage = 1)
    {
        AudioManager.Instance.PlaySound(_lifeDownClip);
        //Temporal para que no me rompa todo
        if (_actualPlayerLives <= 0)
            return;

        ShakeCamera(_timeToShake, _shakingForce);

        _actualPlayerLives -= damage;

        //Por si las moscas
        if (_actualPlayerLives < 0)
            _actualPlayerLives = 0;

        //Fadeout de la vida
        if (damage != 1)
            for (int i = 0; i < _playerLivesImages.Length; i++)
                _playerLivesImages[i].DOFade(0f, 0.5f).SetUpdate(true);
        else
            _playerLivesImages[_actualPlayerLives].DOFade(0f, 0.5f).SetUpdate(true);

        if (_actualPlayerLives <= 0)
            EndGame(false);
    }

    public void ShakeCamera(float time, float force)
    {
        _mainCanvas.renderMode = RenderMode.WorldSpace;
        Camera.main.DOShakePosition(time, force).OnComplete(() => _mainCanvas.renderMode = RenderMode.ScreenSpaceCamera);
    }

    public void ResetCanvasSpace()
    {
        _mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
    }

    public void EndGame(bool winner)
    {
        StartCoroutine(EndGameSequence(1f, winner));
    }

    IEnumerator EndGameSequence(float delay, bool winner)
    {
        yield return new WaitForSeconds(delay);

        _pauseEvent.Invoke();
        _endWinScore.text = LoLManager.Instance.LanguageTexts["score"] + ": ";
        _endScreenSequence = DOTween.Sequence();
        _endScreen.gameObject.SetActive(true);
        _endScreenSequence.SetDelay(1f).OnComplete(() => { _gameEnded = true; _winner = winner; });
        _endScreenSequence.Append(_endScreen.DOFade(0.75f, _timeToFadeScreen)).SetUpdate(true);


        RowManager.Instance.MuteAllRows();
        if (winner)
        {
            _winButton.onClick.AddListener(SceneLoader.Instance.GoToMainMenu);

            _loseButton.gameObject.SetActive(false);
            _winButton.gameObject.SetActive(true);
            AudioManager.Instance.PlaySound(_winClip, 0.6f, AudioChannels.BackgroundMusic);

            _endScreenSequence.Append(_winImage.DOFade(1, _timeToFadeScreen / 2)).SetUpdate(true);
            _endScreenSequence.Append(_kingImage.DOFade(1, _timeToFadeScreen / 2)).SetUpdate(true);
            _endScreenSequence.Append(_winTitle.DOFade(1, _timeToFadeScreen / 2)).SetUpdate(true);
            _endScreenSequence.Append(_endWinScore.DOFade(1, _timeToFadeScreen / 2).SetUpdate(true));
            _endScreenSequence.Append(_endWinScore.DOText(LoLManager.Instance.LanguageTexts["score"] + ": " + _actualMaxScore.ToString("00000"),
                                                        _timeToFadeScreen / 2).SetUpdate(true));
            _endScreenSequence.Append(_winButton.GetComponent<Image>().DOFade(1, _timeToFadeScreen / 2).SetUpdate(true)).OnComplete(() => LoLManager.Instance.EndGame());
            LoLManager.Instance.ResetData();
        }
        else
        {
            _winButton.gameObject.SetActive(false);
            _loseButton.gameObject.SetActive(true);
            AudioManager.Instance.PlaySound(_loseClip);

            _endScreenSequence.Append(_loseImage.DOFade(1, _timeToFadeScreen / 2).OnComplete(() => Time.timeScale = 0)).SetUpdate(true);
            _endScreenSequence.Append(_loseTitle.DOFade(1, _timeToFadeScreen / 2)).SetUpdate(true);
            _endScreenSequence.Append(_endLoseScore.DOFade(1, _timeToFadeScreen / 2).SetUpdate(true));
            _endScreenSequence.Append(_endLoseScore.DOText(LoLManager.Instance.LanguageTexts["score"] + ": " + _actualMaxScore.ToString("00000"),
                                                        _timeToFadeScreen / 2).SetUpdate(true));
            _endScreenSequence.Append(_loseButton.GetComponent<Image>().DOFade(1, _timeToFadeScreen / 2).SetUpdate(true));
        }
    }

    public void Pause()
    {
        _pauseEvent.Invoke();
        AudioManager.Instance.PlaySound(_pauseClip);
        _rowContainer.SetActive(false);
        _tutorialScreen.SetActive(true);
    }

    public void Resume()
    {
        _resumeEvent.Invoke();
        AudioManager.Instance.PlaySound(_pauseClip);
        _rowContainer.SetActive(true);
        _tutorialScreen.SetActive(false);
    }

    public void ResetPlayerLives()
    {
        _actualPlayerLives = _playerMaxLives;
        for (int i = 0; i < _playerLivesImages.Length; i++)
        {
            _playerLivesImages[i].DOFade(1, 0f);
        }
    }

    public void AddScore(int score)
    {
        _actualMaxScore += score;

        DOTween.To(
            () => _actualScore,
            (int value) => _actualScore = value,
            _actualMaxScore,
            1f).OnUpdate(() => _playerScoreText.text = LoLManager.Instance.LanguageTexts["score"] + ": " + _actualScore.ToString("00000"));
    }

    public void ResetLevel()
    {
        _gameEnded = false;

        AudioManager.Instance.PlaySound(_pauseClip);
        Time.timeScale = 1;

        _actualMaxScore = _lastLevelScore;
        _actualScore = _actualMaxScore;
        _playerScoreText.text = LoLManager.Instance.LanguageTexts["score"] + ": " + _actualScore.ToString("00000");

        _endScreen.DOFade(0, _timeToFadeScreen / 2);
        _winImage.DOFade(0, _timeToFadeScreen / 2);
        _winTitle.DOFade(0, _timeToFadeScreen / 2);
        _endWinScore.DOFade(0, _timeToFadeScreen / 2);

        _loseImage.DOFade(0, _timeToFadeScreen / 2);
        _loseTitle.DOFade(0, _timeToFadeScreen / 2);
        _endLoseScore.DOFade(0, _timeToFadeScreen / 2);

        _loseButton.GetComponent<Image>().DOFade(0, _timeToFadeScreen / 2);
        _winButton.GetComponent<Image>().DOFade(0, _timeToFadeScreen / 2);

        LevelManager.Instance.ResetActualLevel();
        LevelManager.Instance.StartLevel();
        _endScreen.gameObject.SetActive(false);
        ResetCanvasSpace();
        _resumeEvent.Invoke();
    }
}
