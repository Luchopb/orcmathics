﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }
    [Header("Tutorial Level")]
    [SerializeField] private InitialLevelTutorial _tutorialLevel;
    [Header("Sound Settings")]
    [SerializeField] private AudioClip _scoreUpClip;
    [SerializeField] private AudioClip _nextWaveClip;
    [SerializeField] private AudioClip _wrongClip;
    [Header("UI Settings")]
    [SerializeField] private Image _levelprogressBar;
    [SerializeField] private float _progressBarTweenTime;
    [SerializeField] private Text _levelprogressText;
    [SerializeField] private Animator _waveAnimator;
    [SerializeField] private Text _levelCounterText;
    [SerializeField] private Text _signCounterText;
    [Header("Level Settings")]
    [SerializeField] private float _timeForLevelToSet;
    [SerializeField] private LevelData[] _levels;

    private int _levelIndex;
    private int _enemyIndex;
    private int _activeEnemies;
    private float _enemiesForLevel;
    private float _killedEnemies;
    private bool _decreaseLevelAlpha;
    private RowController _lastRow;

    public Vector2 GetActualLevelRange()
    {
        return _levels[_levelIndex].LevelRange;
    }

    private void Awake()
    {
        if (Instance != null) Destroy(gameObject);
        Instance = this;

        _levelIndex = LoLManager.Instance._data.levelIndex;

        var auxColor = _levelCounterText.color;
        auxColor.a = 0;
        _levelCounterText.color = auxColor;
    }

    private void Start()
    {
        GameManager.Instance.PauseEvent.AddListener(PauseSpawn);
        GameManager.Instance.ResumeEvent.AddListener(ResumeSpawn);
        _signCounterText.text = LoLManager.Instance.LanguageTexts["wave"] + " " + (_levelIndex + 1).ToString();
        if (PlayerPrefs.GetInt("HidesResult", 0) == 1)
        {
            PlayerController.Instance.HideShaman();
        }
    }

    public void StartTutorialLevel()
    {
        for (int i = 0; i < 3; i++)
        {
            Enemy auxDummy = UnitPoolManager.Instance.GetEnemy(EnemyClass.Dummy);

            var row = RowManager.Instance.GetAvailableRow(new Vector2(-2, 4));
            auxDummy.SetEnemy(row, 0);
            row.SpawnEnemy(auxDummy);
        }

        _tutorialLevel.StartTutorialLevel();
    }

    public void SpawnEnemy()
    {
        CancelInvoke(nameof(SpawnEnemy));
        //Vuelve a invocar el spawn dependiendo del rate de este nivel
        InvokeRepeating(nameof(SpawnEnemy), _levels[_levelIndex].SpawnRate, _levels[_levelIndex].SpawnRate);
        //Busco un enemigo disponible
        var enemyClass = GetEnemyClass();
        //Si retorna error, no hay enemigos disponible
        if (enemyClass == EnemyClass.Error) return;
        //Getteo un enemigo del pool
        Enemy auxEnemy = UnitPoolManager.Instance.GetEnemy(_levels[_levelIndex].Enemies[_enemyIndex].Enemy);
        //Si el enemigo seleccionado puede spawnear, lo spawneo, si no lo devuelvo y vuelvo a intentar
        if (auxEnemy.CanSpawn())
        {
            //Busco un row random
            _lastRow = RowManager.Instance.GetAvailableRow(_levels[_levelIndex].LevelRange);
            //si no esta disponible, retorno
            if (_lastRow == null)
            {
                UnitPoolManager.Instance.ReturnEnemy(auxEnemy);
                StartCoroutine(ReturnToQueue(_levels[_levelIndex].SpawnRate / 2, _enemyIndex));
                SpawnEnemy();
            }
            else
            {
                //Le inyecto sus datos y lo spawneo en su row
                auxEnemy.SetEnemy(_lastRow, _enemyIndex);
                _lastRow.SpawnEnemy(auxEnemy);
                _activeEnemies++;
            }
        }
        else
        {
            UnitPoolManager.Instance.ReturnEnemy(auxEnemy);
            StartCoroutine(ReturnToQueue(_levels[_levelIndex].SpawnRate / 2, _enemyIndex));
            SpawnEnemy();
        }
    }

    IEnumerator ReturnToQueue(float delay, int index)
    {
        yield return new WaitForSeconds(delay);
        _levels[_levelIndex].Enemies[index].Used = false;
    }

    //Agarro el tipo del enemigo libre
    public EnemyClass GetEnemyClass()
    {
        for (int i = 0; i < _levels[_levelIndex].Enemies.Length; i++)
        {
            if (!_levels[_levelIndex].Enemies[i].Used)
            {
                _levels[_levelIndex].Enemies[i].Used = true;
                _enemyIndex = i;
                return _levels[_levelIndex].Enemies[i].Enemy;
            }
        }
        return EnemyClass.Error;
    }

    public void ReturnEnemyToQueue(int index)
    {
        _activeEnemies--;
        _levels[_levelIndex].Enemies[index].Used = false;
    }

    public void EnemyKilled()
    {
        _killedEnemies++;
        _activeEnemies--;

        UpdateLevelUI();

        if (_killedEnemies >= _enemiesForLevel)
            SetNextLevel();
        else if (_activeEnemies <= 0)
            SpawnEnemy();
    }

    public void SetNextLevel()
    {
        _killedEnemies = 0;
        _enemyIndex = 0;
        //ResetActualLevel();
        _levelIndex++;

        //Submits progress to lol sdk
        LoLManager.Instance.SubmitProgress(GameManager.Instance.ActualMaxScore, _levelIndex, _levels.Length);

        CancelInvoke(nameof(SpawnEnemy));
        if (_levelIndex >= _levels.Length)
        {
            GameManager.Instance.EndGame(true);
            return;
        }

        StartCoroutine(StartLevelAfterDelay(1f));
    }

    IEnumerator StartLevelAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Debug.Log($"46543 -> StartLevelAfterDelay");
        if (_levels[_levelIndex].ChangesSign)
        {
            PlayerController.Instance.ChangeSign(true);
            yield break;
        }
        Debug.Log($"465432 -> Hide Result saved value [{PlayerPrefs.GetInt("HidesResult", 0) == 1}]");
        if (_levels[_levelIndex].HidesResult)
        {
            PlayerController.Instance.HideResult();
            yield break;
        }

        StartLevel();
    }

    public void StartLevel()
    {
        if (_levels[_levelIndex].Tutorial != null)
        {
            if (!_levels[_levelIndex].Tutorial.Shown)
            {
                _levels[_levelIndex].Tutorial.Show();
                return;
            }
        }

        //StartTutorialLevel();
        PlayerController.Instance.Resume();
        PlayerController.Instance.ResetPlayer();
        GameManager.Instance.ResetPlayerLives();
        _decreaseLevelAlpha = true;

        Invoke(nameof(PlayNextWaveClip), 0.5f);
        _levelCounterText.text = LoLManager.Instance.LanguageTexts["wave"] + " " + (_levelIndex + 1).ToString();

        _waveAnimator.SetTrigger("NewWave");
        _enemiesForLevel = _levels[_levelIndex].Enemies.Length;
        InvokeRepeating(nameof(SpawnEnemy), _timeForLevelToSet, _levels[_levelIndex].SpawnRate);
        Invoke(nameof(UpdateLevelUI), _timeForLevelToSet / 2);
    }

    private void PlayNextWaveClip()
    {
        AudioManager.Instance.PlaySound(_nextWaveClip, 0.3f);
    }

    private void PauseSpawn()
    {
        CancelInvoke(nameof(SpawnEnemy));
    }

    private void ResumeSpawn()
    {
        if (_tutorialLevel.Shown)
            InvokeRepeating(nameof(SpawnEnemy), _timeForLevelToSet, _levels[_levelIndex].SpawnRate);
    }

    private void UpdateLevelUI()
    {
        if (_levelIndex != 0)
            AudioManager.Instance.PlaySound(_scoreUpClip);

        _signCounterText.text = LoLManager.Instance.LanguageTexts["wave"] + " " + (_levelIndex + 1).ToString();
        _levelprogressBar.DOFillAmount(_killedEnemies / _enemiesForLevel, _progressBarTweenTime);
        _levelprogressText.text = _killedEnemies + "/" + _enemiesForLevel;
    }

    public void ResetActualLevel()
    {
        CancelInvoke(nameof(SpawnEnemy));
        _enemyIndex = 0;
        _killedEnemies = 0;
        //reseteo los enemigos
        for (int i = 0; i < _levels[_levelIndex].Enemies.Length; i++)
        {
            _levels[_levelIndex].Enemies[i].Used = false;
        }

        RowManager.Instance.ResetAllRows();
    }
}

[System.Serializable]
public class LevelData
{
    public float SpawnRate;
    public Vector2 LevelRange;
    public bool ChangesSign;
    public bool HidesResult;
    public LevelTutorial Tutorial;
    [Header("Enemies")]
    public EnemyData[] Enemies;
}
