﻿using LoL.Examples.Cooking;
using LoLSDK;
using SimpleJSON;
using System;
using System.Collections;
using System.IO;
using System.Xml.Linq;
using UnityEngine;

public class LoLManager : MonoBehaviour
{
    public static LoLManager Instance { get; private set; }
    public static event Action OnChangeLanguage = delegate { };

    [SerializeField] private string _startGameFilePath;
    [SerializeField] private string _languageFilePath;

    private JSONNode _languageTexts;

    public JSONNode LanguageTexts { get { return _languageTexts; } }
    public OM_Data _data { get; private set; }

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;
        DontDestroyOnLoad(gameObject);
        Application.runInBackground = false;
    }

    private void Start()
    {

        // Create the WebGL (or mock) object
#if UNITY_EDITOR
        ILOLSDK webGL = new LoLSDK.MockWebGL();
#elif UNITY_WEBGL
        ILOLSDK webGL = new LoLSDK.WebGL();
#endif

        // Initialize the object, passing in the WebGL
        LOLSDK.Init(webGL, "com.gameever.orc-mathics");
        //LOLSDK.Init(webGL, "com.legends-of-learning.unity.sdk.v4.example-game");


        LOLSDK.Instance.StartGameReceived += new StartGameReceivedHandler(StartGame);
        LOLSDK.Instance.LanguageDefsReceived += new LanguageDefsReceivedHandler(SetLanguage);

        LOLSDK.Instance.GameStateChanged += new GameStateChangedHandler(gameState => Debug.Log(gameState));
        LOLSDK.Instance.QuestionsReceived += new QuestionListReceivedHandler(questionList => Debug.Log(questionList));


#if UNITY_EDITOR
        // Mock the platform-to-game messages when in the Unity editor.
        LoadMockData();
#endif

        LOLSDK.Instance.GameIsReady();
        StartCoroutine(LoadLanguageData());
    }

    private void StartGame(string json = "")
    {
        LoadData();
        SceneLoader.Instance.GoToMainMenu();
    }

    // Use language to populate UI
    private void SetLanguage(string json)
    {
        JSONNode langDefs = JSON.Parse(json);
        _languageTexts = langDefs;
        OnChangeLanguage();
    }

    public void SubmitProgress(int score, int actualLevel, int maxLevels)
    {
        LOLSDK.Instance.SubmitProgress(score, actualLevel, maxLevels);
        SaveData(actualLevel,score);
    }

    public void SaveData(int levelIndex, int score)
    {
        if(_data == null) _data = new OM_Data();
        _data.levelIndex = levelIndex;
        _data.score = score;
        LOLSDK.Instance.SaveState(_data);
    }

    public void LoadData()
    {
        LOLSDK.Instance.LoadState<OM_Data>(state =>
        {
            if (state != null)
            {
                _data = state.data;
            }
        });
        if(_data == null)
            _data = new OM_Data();
    }
    public void ResetData()
    {
        if (_data != null)
        {
            _data = new OM_Data();
            LOLSDK.Instance.SaveState(_data);
        }
        PlayerPrefs.DeleteAll();
    }


    public void EndGame()
    {
        LOLSDK.Instance.CompleteGame();
    }

    public void SpeechToText(string key)
    {
        LOLSDK.Instance.SpeakText(key);
    }

    private void LoadMockData()
    {
#if UNITY_EDITOR
        // Load Dev Language File from StreamingAssets
        string startDataFilePath = Path.Combine(Application.streamingAssetsPath, _startGameFilePath);
        string langCode = "en";

        if (File.Exists(startDataFilePath))
        {
            string startDataAsJSON = File.ReadAllText(startDataFilePath);
            JSONNode startGamePayload = JSON.Parse(startDataAsJSON);
            // Capture the language code from the start payload. Use this to switch fontss
            langCode = startGamePayload["languageCode"];
            StartGame();
        }

        // Load Dev Language File from StreamingAssets
        string langFilePath = Path.Combine(Application.streamingAssetsPath, _languageFilePath);
        if (File.Exists(langFilePath))
        {
            string langDataAsJson = File.ReadAllText(langFilePath);
            // The dev payload in language.json includes all languages.
            // Parse this file as JSON, encode, and stringify to mock
            // the platform payload, which includes only a single language.
            JSONNode langDefs = JSON.Parse(langDataAsJson);
            // use the languageCode from startGame.json captured above
            SetLanguage(langDefs[langCode].ToString());
        }
        // Load Dev Questions from StreamingAssets
        /*
        string questionsFilePath = Path.Combine(Application.streamingAssetsPath, questionsJSONFilePath);
        if (File.Exists(questionsFilePath))
        {
            string questionsDataAsJson = File.ReadAllText(questionsFilePath);
            MultipleChoiceQuestionList qs =
                MultipleChoiceQuestionList.CreateFromJSON(questionsDataAsJson);
            HandleQuestions(qs);
        }
        */
#endif
    }

    private IEnumerator LoadLanguageData()
    {
        string langFilePath = Path.Combine(Application.streamingAssetsPath, _languageFilePath);
        string langCode = "en";
        string result;

        //if (File.Exists(langFilePath))
        if (langFilePath.Contains("://") || langFilePath.Contains(":///"))
        {
            WWW www = new WWW(langFilePath);
            yield return www;
            result = www.text;
        }
        else
        {
            result = File.ReadAllText(langFilePath);

        }

        JSONNode langDefs = JSON.Parse(result);
        SetLanguage(langDefs[langCode].ToString());
        StartGame();
    }
}

public class OM_Data
{
    public int levelIndex;
    public int score;
}