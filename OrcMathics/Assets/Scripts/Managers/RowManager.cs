﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowManager : MonoBehaviour
{
    public static RowManager Instance { get; private set; }

    [SerializeField] private int _rowAmount = 15;
    [SerializeField] private RowController _rowPrefab;

    private RectTransform _myRectTransform;
    private int _actualRowIndex;
    
    private List<RowController> _rows;

    public List<RowController> AllRows { get { return _rows; } }

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;

        _actualRowIndex = -_rowAmount / 2;

        _myRectTransform = GetComponent<RectTransform>();
        _rows = new List<RowController>();
    }

    private void Start()
    {
        float rowWidth = _myRectTransform.rect.width / _rowAmount;

        //Spawneo los caminos y los agrego a la lista de disponibles
        for (int i = 0; i < _rowAmount; i++)
        {
            var auxRow = Instantiate(_rowPrefab, transform);
            auxRow.SetRow(rowWidth, _actualRowIndex);
            _actualRowIndex++;
            auxRow.transform.localPosition = new Vector3(auxRow.RectTransform.rect.width * i - (_myRectTransform.rect.width / 2 - rowWidth / 2), 0, 0);
            _rows.Add(auxRow);
        }
    }

    public RowController GetAvailableRow(Vector2 range)
    {
        //Busco las filas que estan dentro del rango, no en la posición del player y desocupadas
        var usableRows = _rows.Where(x => x.RowIndex >= range.x 
                                                && x.RowIndex <= range.y 
                                                && x.RowIndex != PlayerController.Instance.ActualRowIndex
                                                && !x.Occupied).ToList();

        if (usableRows.Count <= 0)
            return null;
        //Dentro de esas filas, retorno una random
        var rowToReturn = usableRows[Random.Range(0, usableRows.Count())];

        return rowToReturn;
    }

    public List<RowController> GetAvailableRows(Vector2 range, int amount)
    {
        var usableRows = _rows.Where(x => x.RowIndex >= range.x
                                                && x.RowIndex <= range.y
                                                && x.RowIndex != PlayerController.Instance.ActualRowIndex
                                                && !x.Occupied).ToList();

        //Si no hay rows suficientes, retorno nulo
        if (usableRows.Count() < amount) return null;

        List<RowController> rowsToReturn = new List<RowController>();

        for (int i = 0; i < amount; i++)
        {
            var row = usableRows.ElementAt(Random.Range(0, usableRows.Count()));

            usableRows.Remove(row);
            rowsToReturn.Add(row);
        }

        return rowsToReturn;
    }

    public int GetAvailableRowsAmount(Vector2 range)
    {
        var usableRows = _rows.Where(x => x.RowIndex >= range.x
                                                       && x.RowIndex <= range.y
                                                       && x.RowIndex != PlayerController.Instance.ActualRowIndex
                                                       && !x.Occupied);
        return usableRows.Count();
    }

    public void ResetAllRows()
    {
        for (int i = 0; i < _rows.Count; i++)
        {
            _rows[i].ResetActiveEnemy();
        }
    }

    public void MuteAllRows()
    {
        for (int i = 0; i < _rows.Count; i++)
        {
            _rows[i].MuteActiveEnemy();
        }
    }

    public RowController CheckForOccupiedRow(int index)
    {
        for (int i = 0; i < _rows.Count; i++)
        {
            if (_rows[i].RowIndex == index && _rows[i].Occupied)
                return _rows[i];
        }
        return null;
    }

    public RowController GetRowByIndex(int index)
    {
        for (int i = 0; i < _rows.Count; i++)
        {
            if (_rows[i].RowIndex == index)
                return _rows[i];
        }
        return null;
    }

    public List<RowController> GetSequentialRows(Vector2 range, int amount)
    {
        //TODO: mejorar esta cabeceada
        var usableRows = _rows.Where(x => x.RowIndex >= range.x
                                                && x.RowIndex < range.y
                                                //&& x.RowIndex != PlayerController.Instance.ActualRowIndex
                                                && !x.Occupied
                                                && _rows.IndexOf(x) + 2 < _rows.Count
                                                && !_rows[_rows.IndexOf(x) + 1].Occupied
                                                && !_rows[_rows.IndexOf(x) + 2].Occupied).ToList();

        if (usableRows.Count() < amount) return null;

        List<RowController> rowsToReturn = new List<RowController>();

        var row = usableRows.ElementAt(Random.Range(0, usableRows.Count()));

        rowsToReturn.Add(row);
        rowsToReturn.Add(_rows[_rows.IndexOf(row) + 1]);
        rowsToReturn.Add(_rows[_rows.IndexOf(row) + 2]);

        return rowsToReturn;
    }
}
