﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UnitPoolManager : MonoBehaviour
{
    public static UnitPoolManager Instance { get; private set; }
    
    [SerializeField] private int _initialBoltAmount;
    [SerializeField] private MagicBoltBehaviour _boltPrefab;
    [Header("Enemies")]
    [SerializeField] private int _initialEnemyAmount;
    [SerializeField] private EnemyDummy _dummyPrefab;
    [SerializeField] private EnemyWarrior _warriorPrefab;
    [SerializeField] private EnemyGoblin _goblinPrefab;
    [SerializeField] private EnemyRider _riderPrefab;
    [SerializeField] private EnemyRogue _roguePrefab;
    [SerializeField] private EnemyShaman _shamanPrefab;
    [SerializeField] private EnemyMonk _monkPrefab;
    [SerializeField] private EnemyWarchief _warchiefPrefab;
    [SerializeField] private EnemyBoss _bossPrefab;

    private List<Enemy> _enemyPool;
    private List<MagicBoltBehaviour> _boltPool;

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;

        _enemyPool = new List<Enemy>();
        _boltPool = new List<MagicBoltBehaviour>();

        for (int i = 0; i < _initialEnemyAmount; i++)
        {
            //Dummy
            var auxDummy = Instantiate(_dummyPrefab, transform);
            _enemyPool.Add(auxDummy);
            auxDummy.gameObject.SetActive(false);
            //Warrior
            var auxWarrior = Instantiate(_warriorPrefab, transform);
            _enemyPool.Add(auxWarrior);
            auxWarrior.gameObject.SetActive(false);
            //Goblin
            var auxGoblin = Instantiate(_goblinPrefab, transform);
            _enemyPool.Add(auxGoblin);
            auxGoblin.gameObject.SetActive(false);
            //Rider
            var auxRider = Instantiate(_riderPrefab, transform);
            _enemyPool.Add(auxRider);
            auxRider.gameObject.SetActive(false);
            //Rogue
            var auxRogue = Instantiate(_roguePrefab, transform);
            _enemyPool.Add(auxRogue);
            auxRogue.gameObject.SetActive(false);
            //Shaman
            var auxShaman = Instantiate(_shamanPrefab, transform);
            _enemyPool.Add(auxShaman);
            auxShaman.gameObject.SetActive(false);
            //Monk
            var auxMonk = Instantiate(_monkPrefab, transform);
            _enemyPool.Add(auxMonk);
            auxMonk.gameObject.SetActive(false);
            //Warchief
            var auxWarchief = Instantiate(_warchiefPrefab, transform);
            _enemyPool.Add(auxWarchief);
            auxWarchief.gameObject.SetActive(false);
        }
        //Boss
        var auxBoss = Instantiate(_bossPrefab, transform);
        _enemyPool.Add(auxBoss);
        auxBoss.gameObject.SetActive(false);

        for (int i = 0; i < _initialBoltAmount; i++)
        {
            var auxBolt = Instantiate(_boltPrefab, transform);
            _boltPool.Add(auxBolt);
            auxBolt.gameObject.SetActive(false);
        }
    }

    public Enemy GetEnemy(EnemyClass enemyClass)
    {
        var enemyToReturn = _enemyPool.Where(x => x.Class == enemyClass && !x.InUse).First();
        enemyToReturn.gameObject.SetActive(true);
        enemyToReturn.InUse = true;

        return enemyToReturn;
    }

    public void ReturnEnemy(Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        enemy.InUse = false;
    }

    public MagicBoltBehaviour GetMagicBolt()
    {
        var boltToReturn = _boltPool.Where(x => !x.isActiveAndEnabled).First();
        boltToReturn.gameObject.SetActive(true);
        return boltToReturn;
    }

    public void ReturnBolt(MagicBoltBehaviour bolt)
    {
        bolt.gameObject.SetActive(false);
    }
}
