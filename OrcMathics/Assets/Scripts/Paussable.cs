﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPaussable 
{
    void Pause();
    void Resume();
}
