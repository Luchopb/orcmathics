﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MagicBoltBehaviour : MonoBehaviour
{
    [SerializeField] private float _speed;

    private Enemy _targetEnemy;
    private Transform _shieldTarget;
    private bool _targettingShield;
    private UnityEvent _killedEnemyEvent;

    private void Awake()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        if ((_targetEnemy == null && !_targettingShield))
        {
            print(_targettingShield);
            gameObject.SetActive(false);
            return;
        }
        /*
        if(_targetEnemy == null)
        {
            gameObject.SetActive(false);
            return;
        }*/

        //Calcula la distancia al escudo o al enemigo
        var actualDistance = Vector2.Distance(transform.position, _targettingShield ? _shieldTarget.position : _targetEnemy.transform.position);
        //transform.LookAt(_targetEnemy.transform);

        //transform.rotation = Quaternion.LookRotation(_targetEnemy.transform.position - transform.position, transform.up);
        if (actualDistance > 0.25f)
            if(!_targettingShield)
                transform.position += (_targetEnemy.transform.position - transform.position).normalized * _speed * Time.deltaTime;
            else 
                transform.position += (_shieldTarget.position - transform.position).normalized * _speed * Time.deltaTime;
        else
            HitTarget();
    }

    public void SetTarget(Enemy target, Vector3 spawnPosition, Transform shieldTransform = null, UnityEvent killedEnemyEvent = null)
    {
        gameObject.SetActive(true);
        transform.position = spawnPosition;

        _shieldTarget = shieldTransform;
        _killedEnemyEvent = killedEnemyEvent;

        if (_shieldTarget == null)
        {
            _targettingShield = false;
            _targetEnemy = target;
        }
        else
            _targettingShield = true;
    }

    public void HitTarget()
    {
        if (!_targettingShield)
        {
            _killedEnemyEvent.Invoke();
            _targetEnemy.KillEnemy(true);
        }
        else
            _shieldTarget.GetComponentInParent<RowController>().HitShield();

        gameObject.SetActive(false);
    }
}
