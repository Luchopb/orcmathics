﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance { get; private set; }

    [SerializeField] private Transform _boltSpawn;
    [SerializeField] private ParticleSystem _goodParticle;
    [SerializeField] private Animator _moveParticle;
    [Header("Audio Settings")]
    [SerializeField] private AudioClip _moveClip;
    [SerializeField] private AudioClip _boltClip;
    [SerializeField] private AudioClip _failClip;
    [Header("Horizontal Scroll")]
    [SerializeField] private RectTransform _scrollUnit;
    [SerializeField] private Text[] _textNumbers;
    [SerializeField] private Text _positionText;
    [SerializeField] private Text _resultText;
    [SerializeField] private Color _selectedColor;
    [SerializeField] private Color _deselectedColor;
    [SerializeField] private ScrollRect _myScroll;
    [SerializeField] private Image _goButton;
    [SerializeField] private Sprite _normalGoButton;
    [SerializeField] private Sprite _pressedGoButton;
    [SerializeField] private AudioClip _scrollAudioClip;
    [Header("Hide Result Pieces")]
    [SerializeField] private Text _equalsText;
    [SerializeField] private Image _thunderImage;
    [SerializeField] private Image _whiteScreenImage;
    [SerializeField] private Animator _shamanImage;
    [SerializeField] private Animator _cloudAnim;
    [SerializeField] private Image _brokenResultImage;
    [SerializeField] private Image _rubbleImage;
    [SerializeField] private AudioClip _thunderClip;
    [Header("Change Sign Pieces")]
    [SerializeField] private Text _signText;
    [SerializeField] private Animator _changeSignAnim;
    [SerializeField] private Animator _bossSignAnim;
    [SerializeField] private ParticleSystem _positiveParticles;
    [SerializeField] private ParticleSystem _negativeParticles;
    [SerializeField] private float _bossFinalPositionY;
    [SerializeField] private AudioClip _changeSignClip;
    [SerializeField] private AudioClip _landBossClip;
    [SerializeField] private Color _positiveColor;
    [SerializeField] private Color _negativeColor;

    private Image _playerImage;
    private RectTransform _content;
    private VerticalLayoutGroup _contentLayoutGroup;
    private Text _actualNumberSelected;
    private bool _positiveSign = true;
    private bool _firing;
    private bool _paused = true;
    private int _selectedNumber;
    private int _actualRowIndex;
    private int _resultIndex;
    private Animator _anim;
    private MagicBoltBehaviour _actualBolt;
    private RowController _actualRow;
    private float _initialBossPositionY;
    private RowController _auxRow;
    private RowEvent _aimRowEvent;
    private UnityEvent _enemyKilledEvent;

    private bool _canIncrease =>
        Input.GetKeyDown(KeyCode.RightArrow) ||
        Input.GetKeyDown(KeyCode.UpArrow) ||
        Input.GetKeyDown(KeyCode.W) ||
        Input.GetKeyDown(KeyCode.D) ||
        Input.GetAxis("Mouse ScrollWheel") > 0;
    private bool _canDecrease =>
        Input.GetKeyDown(KeyCode.LeftArrow) ||
        Input.GetKeyDown(KeyCode.DownArrow) ||
        Input.GetKeyDown(KeyCode.S) ||
        Input.GetKeyDown(KeyCode.A) ||
        Input.GetAxis("Mouse ScrollWheel") < 0;
    private int _actualValue => _actualRowIndex + (_selectedNumber * (_positiveSign ? 1 : -1));

    public int ActualRowIndex { get { return _actualRowIndex; } }
    public bool PositiveSign { get { return _positiveSign; } set { _positiveSign = value; } }
    public RowEvent ChoooseRowEvent { get { return _aimRowEvent; } }
    public UnityEvent EnemyKilledEvent { get { return _enemyKilledEvent; } }

    private void Awake()
    {
        if (Instance != null) Destroy(gameObject);

        Instance = this;
        _anim = GetComponent<Animator>();
        _playerImage = GetComponent<Image>();
        _aimRowEvent = new RowEvent();
        _enemyKilledEvent = new UnityEvent();

        _content = _myScroll.content;
        _contentLayoutGroup = _content.GetComponent<VerticalLayoutGroup>();
        _actualNumberSelected = _textNumbers[14];
        _positionText.text = _actualRowIndex.ToString();
        _resultText.text = _actualValue.ToString();
        _positiveSign = PlayerPrefs.GetInt("positiveSign", 1) == 1;
        SetTextSign(DOTween.Sequence(), true);
    }

    private void Start()
    {
        _initialBossPositionY = _bossSignAnim.transform.localPosition.y;
        GameManager.Instance.PauseEvent.AddListener(Pause);
        GameManager.Instance.ResumeEvent.AddListener(Resume);
    }

    private void Update()
    {
        if (_paused) return;

        if (_canIncrease)
        {
            _selectedNumber++;
            SnapToNumber(true);
        }
        else if (_canDecrease)
        {
            _selectedNumber--;
            SnapToNumber(true);
        }


        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            MovePlayerToRow();

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.C))
            ChangeSign(true);

        if (Input.GetKeyDown(KeyCode.H))
            HideResult();
#endif
    }

    public void UpDownNumber(int amount)
    {
        _selectedNumber += amount;
        SnapToNumber(true);
    }

    public void SnapToNumber(bool keyboardInput = false)
    {
        AudioManager.Instance.PlaySound(_scrollAudioClip);
        if (!keyboardInput)
            _selectedNumber = Mathf.RoundToInt(_content.localPosition.y / (_scrollUnit.rect.height + _contentLayoutGroup.spacing));

        _selectedNumber = Mathf.Clamp(_selectedNumber, -14, 14);

        _resultIndex = _actualValue;

        _actualNumberSelected.fontSize = 24;
        _actualNumberSelected.color = _deselectedColor;
        _actualNumberSelected = _textNumbers[_selectedNumber + 14];

        if (_resultIndex <= 7 && _resultIndex >= -7)
        {
            _actualNumberSelected.fontSize = 32;
            _actualNumberSelected.color = _selectedColor;
        }

        _resultText.text = _resultIndex.ToString();

        _auxRow = RowManager.Instance.CheckForOccupiedRow(_actualValue);
        _aimRowEvent.Invoke(_auxRow);
        _content.DOLocalMoveY(_selectedNumber * (_scrollUnit.rect.height + _contentLayoutGroup.spacing), 0.25f).SetEase(Ease.OutQuad);
    }

    public void MovePlayerToRow(bool forceMove = false)
    {
        if (_firing) return;

        _firing = true;
        _goButton.sprite = _pressedGoButton;

        if (_auxRow && _auxRow.ActiveEnemy != null && !_auxRow.ActiveEnemy.AimedAt)
        {
            _actualRow = _auxRow;

            //_playerImage.enabled = false;
            //transform.DOLocalMoveX(_actualRow.transform.localPosition.x, 0.1f).SetEase(Ease.OutQuad).OnComplete(FireMagicBolt);
            if (!_auxRow.Immune)
                _actualRow.ActiveEnemy.AimedAt = true;

            _moveParticle.SetTrigger("Teleport");
            _moveParticle.transform.position = transform.position;

            AudioManager.Instance.PlaySound(_moveClip);
            _anim.SetTrigger("Move");
            transform.DOLocalMoveX(_actualRow.transform.localPosition.x, 0.25f).OnComplete(FireMagicBolt);

            if (_positiveSign)
                _actualRowIndex += _selectedNumber;
            else
                _actualRowIndex -= _selectedNumber;

            _positionText.text = _actualRowIndex.ToString();

            _selectedNumber = 0;
            SnapToNumber(true);
        }
        else
        {
            _anim.SetTrigger("Fail");
            Invoke(nameof(PlayFailClip), 0.25f);
            _selectedNumber = 0;
            SnapToNumber(true);
        }
    }

    public void PlayFailClip()
    {
        AudioManager.Instance.PlaySound(_failClip);
    }

    public void FireMagicBolt()
    {
        //_playerImage.enabled = true;
        _anim.SetTrigger("Shoot");
        AudioManager.Instance.PlaySound(_boltClip, 0.3f);
        Invoke(nameof(SpawnMagicBolt), 0.35f);
    }

    public void SpawnMagicBolt()
    {
        _actualBolt = UnitPoolManager.Instance.GetMagicBolt();
        _actualBolt.SetTarget(_actualRow.ActiveEnemy, _boltSpawn.position, _actualRow.Immune ? _actualRow.ImmuneImage.transform : null, _enemyKilledEvent);
        _goodParticle.Play();
        ResetFiring();
    }

    public void ResetFiring()
    {
        _firing = false;
        _goButton.sprite = _normalGoButton;
    }

    public void ChangeSign(bool levelAnimation = false)
    {
        if (levelAnimation)
        {
            Sequence bossSequence = DOTween.Sequence();
            bossSequence.PrependInterval(0.5f);
            bossSequence.Append(_bossSignAnim.transform.DOLocalMoveY(_bossFinalPositionY, 1.25f).SetEase(Ease.OutQuad).OnComplete(LandBoss));
            bossSequence.Append(_bossSignAnim.transform.DOLocalMoveY(_initialBossPositionY, 1.25f).SetDelay(3.75f).SetEase(Ease.OutQuad));
            StartCoroutine(CallChangeSign(4.25f));
        }
        else
        {
            ChangeSignAnimation(false);
        }
    }

    public void LandBoss()
    {
        _bossSignAnim.SetTrigger("Sign");
        AudioManager.Instance.PlaySound(_landBossClip);
    }

    IEnumerator CallChangeSign(float delay)
    {
        yield return new WaitForSeconds(delay);
        ChangeSignAnimation(true);
    }

    private void ChangeSignAnimation(bool levelAnimation = true)
    {
        AudioManager.Instance.PlaySound(_changeSignClip);

        _positiveSign = !_positiveSign;
        _changeSignAnim.GetComponent<Image>().color = _positiveSign ? _positiveColor : _negativeColor;
        _changeSignAnim.SetTrigger("Effect");
        Sequence changeSignSeq = DOTween.Sequence();
        if (levelAnimation)
        {
            GameManager.Instance.ShakeCamera(1f, 0.1f);
            changeSignSeq.OnComplete(() => LevelManager.Instance.StartLevel());
        }
        SetTextSign(changeSignSeq);
        PlayerPrefs.SetInt("positiveSign", _positiveSign ? 1 : 0);
        _resultText.text = _actualValue.ToString();
    }

    private void SetTextSign(Sequence changeSignSeq, bool instant = false)
    {
        if (_positiveSign)
        {
            _signText.text = "+";
            if (instant)
            {
                _signText.color = _positiveColor;
            }
            else
            {
                _positiveParticles.Play();
                changeSignSeq.Append(_signText.DOColor(_positiveColor, 1.5f));
            }
        }
        else
        {
            _signText.text = "-";
            if (instant)
            {
                _signText.color = _negativeColor;
            }
            else
            {
                _negativeParticles.Play();
                changeSignSeq.Append(_signText.DOColor(_negativeColor, 1.5f));
            }
        }
        _selectedNumber = 0;
        SnapToNumber(true);
    }

    public void HideResult()
    {
        _shamanImage.SetTrigger("Cast");
        //Aparece chaman, aparece trueno, pantalla blanca, aparece roto el resultado
        Sequence hideSequence = DOTween.Sequence().OnComplete(() => LevelManager.Instance.StartLevel());
        hideSequence.AppendInterval(2f);
        hideSequence.Append(_thunderImage.DOFillAmount(1, 0.65f));
        hideSequence.Append(_whiteScreenImage.DOFade(1, 0.01f).OnComplete(() => HideShaman()));
        hideSequence.AppendInterval(0.05f);
        hideSequence.Append(_whiteScreenImage.DOFade(0, 0.01f));
        hideSequence.Append(_whiteScreenImage.DOFade(1, 0.01f));
        hideSequence.AppendInterval(0.05f);
        hideSequence.Append(_whiteScreenImage.DOFade(0, 0.01f));
        PlayerPrefs.SetInt("HidesResult", 1);
        Debug.Log($"465432 -> HideResult()");
    }

    public void HideShaman(bool instant = false)
    {
        if (instant)
        {
            _brokenResultImage.fillAmount = 1;
        }
        else
        {
            AudioManager.Instance.PlaySound(_thunderClip);
            _brokenResultImage.DOFillAmount(1, 1f);
            _cloudAnim.SetTrigger("Cloud");
            _thunderImage.DOFillAmount(0, 0f);
            _rubbleImage.enabled = true;
            _rubbleImage.transform.DOMoveY(transform.position.y - 5, 1.5f);
        }
        _resultText.DOFade(0f, 0f);
        _equalsText.DOFade(0f, 0f);
        Debug.Log($"465432 -> HideShaman()");
    }

    public void ResetPlayer()
    {
        _firing = false;
        _selectedNumber = 0;
        _actualRowIndex = 0;
        _positionText.text = _actualRowIndex.ToString();
        SnapToNumber(true);
        _moveParticle.SetTrigger("Teleport");
        _moveParticle.transform.position = transform.position;
        var row = RowManager.Instance.GetRowByIndex(0);
        transform.localPosition = new Vector3(row.transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
    }

    public void Pause()
    {
        _paused = true;
    }

    public void Resume()
    {
        _paused = false;
    }
}

public class RowEvent : UnityEvent<RowController> { }