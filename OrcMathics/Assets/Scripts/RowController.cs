﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RowController : MonoBehaviour
{
    [SerializeField] private RectTransform _spawnPosition;
    [SerializeField] private RectTransform _endPosition;
    [Header("Shield Settings")]
    [SerializeField] private Image _immuneImage;
    [SerializeField] private Color _shieldNormalColor;
    [SerializeField] private Color _shieldHitColor;
    [SerializeField] private Animator _hitAnimator;
    [SerializeField] private AudioClip _hitShieldClip;

    private RectTransform _rectTransform;
    private Enemy _activeEnemy;
    private int _rowIndex;
    private bool _occupied;
    private bool _immune;

    public RectTransform RectTransform { get { return _rectTransform; } }
    public RectTransform SpawnPosition { get { return _spawnPosition; } }
    public RectTransform EndPosition { get { return _endPosition; } }
    public Enemy ActiveEnemy { get { return _activeEnemy; } }
    public int RowIndex { get { return _rowIndex; } }
    public bool Immune { get { return _immune; } set { _immune = value; } }
    public bool Occupied { get { return _occupied; } set { _occupied = true; } }
    public Image ImmuneImage { get { return _immuneImage; } }

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

    public void SetRow(float width, int index)
    {
        //_spawnPosition.sizeDelta = new Vector2(width, width);
        //_endPosition.sizeDelta = new Vector2(width, width);
        _rectTransform.sizeDelta = new Vector2(width, width * 2 + transform.parent.GetComponent<RectTransform>().rect.height);
        _endPosition.transform.localPosition += new Vector3(0, width * 1.75f, 0);
        _immuneImage.transform.localPosition += new Vector3(0, width * 3.5f, 0);
        _immuneImage.color = _shieldNormalColor;
        _immuneImage.DOFade(0f, 0f);
        _rowIndex = index;
    }

    public void SpawnEnemy(Enemy enemy)
    {
        _activeEnemy = enemy;
        _activeEnemy.transform.position = _spawnPosition.position;
        _activeEnemy.Spawn();
    }

    //Mato al enemigo y me libero
    public void ResetActiveEnemy(bool enemyKilled = false)
    {
        if(_activeEnemy != null)
            _activeEnemy.KillEnemy(enemyKilled);

        ReleaseRow();
    }

    public void MuteActiveEnemy()
    {
        if (_activeEnemy != null)
            _activeEnemy.PauseSound();
    }

    //Me marco como disponible
    public void ReleaseRow()
    {
        _activeEnemy = null;
        _occupied = false;
    }

    public void OccupyRow(Enemy enemy)
    {
        _activeEnemy = enemy;
        _occupied = true;
    }

    public void MakeImmune()
    {
        if (_immune) return;

        if (_activeEnemy != null)
        {
            var possibleShaman = _activeEnemy.GetComponent<EnemyShaman>();
            if (possibleShaman != null)
                return;
            _activeEnemy.AimedAt = false;
        }

        _immune = true;
        _immuneImage.DOFade(1f, 0.5f);
    }

    public void TurnOffImmune()
    {
        if (!_immune) return;

        _immune = false;
        _immuneImage.color = _shieldNormalColor;
        _immuneImage.DOFade(0f, 0.5f);
    }

    public void HitShield()
    {
        AudioManager.Instance.PlaySound(_hitShieldClip);
        _hitAnimator.SetTrigger("Hit");
        Sequence colorSeq = DOTween.Sequence();
        colorSeq.Append(_immuneImage.DOColor(_shieldHitColor, 0.25f));
        colorSeq.Append(_immuneImage.DOColor(_shieldNormalColor, 0.25f).SetEase(Ease.InBack));
    }
}
