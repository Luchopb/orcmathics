﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    [Header("Sound Settings")]
    [SerializeField] private AudioClip _switchPageClip;
    [SerializeField] private AudioClip _startGameClip;
    [Header("General Settings")]
    [SerializeField] private Button _resumeButton;
    [SerializeField] private GameObject[] _tutorialPages;
    [SerializeField] private Button _nextButton;
    [SerializeField] private Button _previousButton;
    [SerializeField] private Button _wandButton;
    [SerializeField] private float _timeForNextButton;
    [Header("Tutorial 1")]
    [SerializeField] private Text _tutorialText_1_1;
    [SerializeField] private Text _tutorialText_1_2;
    [Header("Tutorial 2")]
    [SerializeField] private Text _tutorialText_2_1;
    [SerializeField] private Text _tutorialText_2_2;
    [SerializeField] private Text _tutorialText_2_3;
    [SerializeField] private Text _tutorialText_2_4;
    [SerializeField] private Text _tutorialText_2_5;
    [SerializeField] private Text _tutorialText_2_6;
    [Header("Tutorial 3")]
    [SerializeField] private Text _tutorialText_3_1;
    [SerializeField] private Text _tutorialText_3_2;
    [SerializeField] private Text _tutorialText_3_3;
    [SerializeField] private Text _tutorialText_3_4;
    [Header("Tutorial 4")]
    [SerializeField] private Text _tutorialText_4_1;
    [SerializeField] private Text _tutorialText_4_2;
    [SerializeField] private Text _tutorialText_4_3;
    [Header("Tutorial 5")]
    [SerializeField] private Text _tutorialText_5_1;
    [SerializeField] private Text _tutorialText_5_2;
    [SerializeField] private Text _tutorialText_5_3;
    [SerializeField] private Text _tutorialText_5_4;
    [SerializeField] private Text _tutorialText_5_5;
    [SerializeField] private Text _tutorialText_5_6;
    [SerializeField] private Text _tutorialText_5_7;
    [Header("Tutorial 6")]
    [SerializeField] private Text _tutorialText_6_1;
    [SerializeField] private Text _tutorialText_6_2;
    [SerializeField] private Text _tutorialText_6_3;
    [SerializeField] private Text _tutorialText_6_4;
    [SerializeField] private Text _tutorialText_6_5;
    [SerializeField] private Text _tutorialText_6_6;
    [SerializeField] private Text _tutorialText_6_7;
    [SerializeField] private Text _tutorialText_6_8;
    [Header("Tutorial 7")]
    [SerializeField] private Text _tutorialText_7_1;
    [SerializeField] private Text _tutorialText_7_2;
    [SerializeField] private Text _tutorialText_7_3;
    [SerializeField] private Text _tutorialText_7_4;
    [SerializeField] private Text _tutorialText_7_5;
    [Header("Tutorial 8")]
    [SerializeField] private Text _tutorialText_8_1;
    [SerializeField] private Text _tutorialText_8_2;
    [SerializeField] private Text _tutorialText_8_3;
    [SerializeField] private Text _tutorialText_8_4;
    [SerializeField] private Text _tutorialText_8_5;
    [Header("Tutorial 9")]
    [SerializeField] private Text _tutorialText_9_1;
    [SerializeField] private Text _tutorialText_9_2;
    [SerializeField] private Text _tutorialText_9_3;

    private int _index;
    private bool _canGoToNextPage = true;
    private GameObject _activePage;

    private void Awake()
    {
        _nextButton.onClick.AddListener(NextPage);
        _previousButton.onClick.AddListener(PreviousPage);
        _wandButton.onClick.AddListener(StartGame);

        _previousButton.gameObject.SetActive(false);

        for (int i = 0; i < _tutorialPages.Length; i++)
        {
            _tutorialPages[i].SetActive(false);
        }
    }

    private void Start()
    {
        _activePage = _tutorialPages[_index];
        _activePage.gameObject.SetActive(true);
        _resumeButton.gameObject.SetActive(PlayerPrefs.GetInt("Tutorial", 0) != 0);

        UpdateTexts();

        LoLManager.Instance.SpeechToText("speech_tut_1");
    }

    private void UpdateTexts()
    {
        _tutorialText_1_1.text = LoLManager.Instance.LanguageTexts["tut_1_1"];
        _tutorialText_1_2.text = LoLManager.Instance.LanguageTexts["tut_1_2"];

        _tutorialText_2_1.text = LoLManager.Instance.LanguageTexts["tut_2_1"];
        _tutorialText_2_2.text = LoLManager.Instance.LanguageTexts["tut_2_2"];
        _tutorialText_2_3.text = LoLManager.Instance.LanguageTexts["tut_2_3"];
        _tutorialText_2_4.text = LoLManager.Instance.LanguageTexts["tut_2_4"];
        _tutorialText_2_5.text = LoLManager.Instance.LanguageTexts["tut_2_5"];
        _tutorialText_2_6.text = LoLManager.Instance.LanguageTexts["tut_2_6"];

        _tutorialText_3_1.text = LoLManager.Instance.LanguageTexts["tut_3_1"];
        _tutorialText_3_2.text = LoLManager.Instance.LanguageTexts["tut_3_2"];
        _tutorialText_3_3.text = LoLManager.Instance.LanguageTexts["tut_3_3"];
        _tutorialText_3_4.text = LoLManager.Instance.LanguageTexts["tut_3_4"];

        _tutorialText_4_1.text = LoLManager.Instance.LanguageTexts["tut_4_1"];
        _tutorialText_4_2.text = LoLManager.Instance.LanguageTexts["tut_4_2"];
        _tutorialText_4_3.text = LoLManager.Instance.LanguageTexts["tut_4_3"];

        _tutorialText_5_1.text = LoLManager.Instance.LanguageTexts["tut_5_1"];
        _tutorialText_5_2.text = LoLManager.Instance.LanguageTexts["tut_5_2"];
        _tutorialText_5_3.text = LoLManager.Instance.LanguageTexts["tut_5_3"];
        _tutorialText_5_4.text = LoLManager.Instance.LanguageTexts["tut_5_4"];
        _tutorialText_5_5.text = LoLManager.Instance.LanguageTexts["tut_5_5"];
        _tutorialText_5_6.text = LoLManager.Instance.LanguageTexts["tut_5_6"];
        _tutorialText_5_7.text = LoLManager.Instance.LanguageTexts["tut_5_7"];

        _tutorialText_6_1.text = LoLManager.Instance.LanguageTexts["tut_6_1"];
        _tutorialText_6_2.text = LoLManager.Instance.LanguageTexts["tut_6_2"];
        _tutorialText_6_3.text = LoLManager.Instance.LanguageTexts["tut_6_3"];
        _tutorialText_6_4.text = LoLManager.Instance.LanguageTexts["tut_6_4"];
        _tutorialText_6_5.text = LoLManager.Instance.LanguageTexts["you"];
        _tutorialText_6_6.text = LoLManager.Instance.LanguageTexts["movement"];
        _tutorialText_6_7.text = LoLManager.Instance.LanguageTexts["dest"];
        _tutorialText_6_8.text = LoLManager.Instance.LanguageTexts["sign"];

        _tutorialText_7_1.text = LoLManager.Instance.LanguageTexts["tut_7_1"];
        _tutorialText_7_2.text = LoLManager.Instance.LanguageTexts["you"];
        _tutorialText_7_3.text = LoLManager.Instance.LanguageTexts["movement"];
        _tutorialText_7_4.text = LoLManager.Instance.LanguageTexts["dest"];
        _tutorialText_7_5.text = LoLManager.Instance.LanguageTexts["sign"];

        _tutorialText_8_1.text = LoLManager.Instance.LanguageTexts["tut_8_1"];
        _tutorialText_8_2.text = LoLManager.Instance.LanguageTexts["you"];
        _tutorialText_8_3.text = LoLManager.Instance.LanguageTexts["movement"];
        _tutorialText_8_4.text = LoLManager.Instance.LanguageTexts["dest"];
        _tutorialText_8_5.text = LoLManager.Instance.LanguageTexts["sign"];

        _tutorialText_9_1.text = LoLManager.Instance.LanguageTexts["tut_9_1"];
        _tutorialText_9_2.text = LoLManager.Instance.LanguageTexts["tut_9_2"];
        _tutorialText_9_3.text = LoLManager.Instance.LanguageTexts["tut_9_3"];
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.RightArrow))
            NextPage();

        if (Input.GetKeyDown(KeyCode.LeftArrow))
            PreviousPage();
    }

    private void OnEnable()
    {
        _resumeButton.gameObject.SetActive(PlayerPrefs.GetInt("Tutorial", 0) != 0);

        LoLManager.OnChangeLanguage -= UpdateTexts;
        LoLManager.OnChangeLanguage += UpdateTexts;
    }
    private void OnDisable()
    {
        LoLManager.OnChangeLanguage -= UpdateTexts;
    }

    public void NextPage()
    {
        if (!_canGoToNextPage) return;

        if (_index + 1 >= _tutorialPages.Length)
        {
            StartGame();
            return;
        }

        _index++;
        if (_index + 1 >= _tutorialPages.Length)
        {
            _nextButton.gameObject.SetActive(false);

            SwitchPage();
        }
        else
        {
            if (PlayerPrefs.GetInt("Tutorial", 0) == 0)
            {
                CancelInvoke(nameof(ActivateNextButton));
                _nextButton.gameObject.SetActive(false);
                _canGoToNextPage = false;
                Invoke(nameof(ActivateNextButton), _timeForNextButton);
            }
            _previousButton.gameObject.SetActive(true);
            SwitchPage();
        }
    }

    public void PreviousPage()
    {
        _index--;
        if (_index < 0)
            _index = 0;
        else
        {
            if (_index == 0)
                _previousButton.gameObject.SetActive(false);

            _nextButton.gameObject.SetActive(true);
            SwitchPage();
        }
    }

    public void SwitchPage()
    {
        _activePage.SetActive(false);
        _activePage = _tutorialPages[_index];
        switch (_index)
        {
            case 0:
                LoLManager.Instance.SpeechToText("speech_tut_1");
                break;
            case 1:
                LoLManager.Instance.SpeechToText("speech_tut_2");
                break;
            case 2:
                LoLManager.Instance.SpeechToText("speech_tut_3");
                break;
            case 3:
                LoLManager.Instance.SpeechToText("speech_tut_4");
                break;
            case 4:
                LoLManager.Instance.SpeechToText("speech_tut_5");
                break;
            case 5:
                LoLManager.Instance.SpeechToText("speech_tut_6");
                break;
            case 6:
                LoLManager.Instance.SpeechToText("speech_tut_7");
                break;
            case 7:
                LoLManager.Instance.SpeechToText("speech_tut_8");
                break;
            case 8:
                LoLManager.Instance.SpeechToText("speech_tut_9");
                break;

            default:
                break;
        }

        AudioManager.Instance.PlaySound(_switchPageClip);
        _activePage.SetActive(true);
    }

    private void ActivateNextButton()
    {
        _canGoToNextPage = true;
        _nextButton.gameObject.SetActive(true);
    }

    public void StartGame()
    {
        LoLManager.Instance.SpeechToText("stop_speech");

        if (PlayerPrefs.GetInt("Tutorial", 0) == 0)
        {
            PlayerPrefs.SetInt("Tutorial", 1);
            LevelManager.Instance.StartTutorialLevel();
            gameObject.SetActive(false);
        }
        else
            GameManager.Instance.Resume();

        AudioManager.Instance.PlaySound(_startGameClip);
    }
}
