﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeTween : MonoBehaviour
{
    [SerializeField] private float _timeToFade;
    [SerializeField] private float _delayBetweenFades = 1f;
    [Range(0, 1)]
    [SerializeField] private float _minFadeValue;
    [Range(0, 1)]
    [SerializeField] private float _maxFadeValue;

    private Image _myImage;

    private void Awake()
    {
        _myImage = GetComponent<Image>();
    }

    void Start()
    {
        Sequence fadeSequence = DOTween.Sequence().SetLoops(int.MaxValue, LoopType.Yoyo);
        fadeSequence.Append(_myImage.DOFade(_minFadeValue, _timeToFade).SetEase(Ease.InOutQuad));
        fadeSequence.AppendInterval(_delayBetweenFades);
        fadeSequence.Append(_myImage.DOFade(_maxFadeValue, _timeToFade).SetEase(Ease.InOutQuad));
        fadeSequence.AppendInterval(_delayBetweenFades);
    }
}
