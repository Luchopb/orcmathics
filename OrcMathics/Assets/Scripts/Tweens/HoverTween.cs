﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HoverTween : MonoBehaviour
{
    [SerializeField] private float _hoverYOffset;

    void Start()
    {
        transform.DOMoveY(transform.position.y + _hoverYOffset, 2f).SetEase(Ease.InOutQuad).SetLoops(int.MaxValue, LoopType.Yoyo);
    }
}
